# Change the IP
IP=192.168.1.113
scp ./* root@$IP:/root


# By default, we'll login and run the program
ssh root@$IP chmod +x /root/*
ssh root@$IP bash /root/main.sh
# If you want to make changes before install comment out the above line

# `dhcpcd` is running by default. To change that, follow the below:

# for wireless, manually run on the device:
# ```
# xbps-install -Sy NetworkManager
# nmtui
# ```
# for next setting NetworkManager for install, change network.conf

# if you're wired and want a static ip:
# ```
# ip link set dev eth0
# ip addr add 192.168.1.10/24 brd + dev eth0
# ip route add default via 192.168.1.254
# ```
# for next setting static ip for install, change network.conf
