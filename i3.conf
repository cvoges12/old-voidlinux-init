font pango:monospace 8
floating_modifier Mod1
new_window pixel 5
new_float pixel 2
hide_edge_borders none
force_focus_wrapping no
focus_follows_mouse yes
focus_on_window_activation smart
mouse_warping output
workspace_layout default
workspace_auto_back_and_forth no

client.focused #4c7899 #285577 #ffffff #2e9ef4 #285577
client.focused_inactive #333333 #5f676a #ffffff #484e50 #5f676a
client.unfocused #333333 #222222 #888888 #292d2e #222222
client.urgent #2f343a #900000 #ffffff #900000 #900000
client.placeholder #000000 #0c0c0c #ffffff #000000 #0c0c0c
client.background #ffffff

set $MOD Mod1
bindsym $MOD+Shift+q exec i3-msg exit
bindsym $MOD+Shift+r restart
bindsym $MOD+q exec i3lock

bindsym $MOD+d exec --no-startup-id dmenu_run
bindsym $MOD+Return exec kitty

bindsym $MOD+w kill
bindsym $MOD+f fullscreen toggle
bindsym $MOD+r mode resize
bindsym $MOD+bar split h
bindsym $MOD+minus split v

bindsym $MOD+space focus mode_toggle
bindsym $MOD+Shift+space floating toggle

bindsym $MOD+s layout stacking
bindsym $MOD+t layout tabbed
bindsym $MOD+v layout toggle split

bindsym $MOD+p focus parent
bindsym $MOD+c focus child

# Workspaces
bindsym $MOD+grave workspace 0
bindsym $MOD+1 workspace 1
bindsym $MOD+2 workspace 2
bindsym $MOD+3 workspace 3
bindsym $MOD+4 workspace 4
bindsym $MOD+5 workspace 5
bindsym $MOD+6 workspace 6
bindsym $MOD+7 workspace 7
bindsym $MOD+8 workspace 8
bindsym $MOD+9 workspace 9
bindsym $MOD+0 workspace 10
bindsym $MOD+Shift+grave move container to workspace 0
bindsym $MOD+Shift+1 move container to workspace 1
bindsym $MOD+Shift+2 move container to workspace 2
bindsym $MOD+Shift+3 move container to workspace 3
bindsym $MOD+Shift+4 move container to workspace 4
bindsym $MOD+Shift+5 move container to workspace 5
bindsym $MOD+Shift+6 move container to workspace 6
bindsym $MOD+Shift+7 move container to workspace 7
bindsym $MOD+Shift+8 move container to workspace 8
bindsym $MOD+Shift+9 move container to workspace 9
bindsym $MOD+Shift+0 move container to workspace 10

gaps inner 10
gaps outer 5
smart_gaps on
smart_borders on

bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume 0 -1%
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute 0 toggle
bindsym XF86AudioNext exec playerctl next
bindsym XF86AudioPause exec playerctl pause
bindsym XF86AudioPrev exec playerctl previous
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume 0 +1%
bindsym XF86MonBrightnessDown exec xbacklight -dec 1
bindsym XF86MonBrightnessUp exec xbacklight -inc 1

# These files are for keyboard specific additions to i3.conf
bindsym $MOD+h focus left
bindsym $MOD+n focus down
bindsym $MOD+e focus up
bindsym $MOD+i focus right
bindsym $MOD+Shift+h move left
bindsym $MOD+Shift+n move down
bindsym $MOD+Shift+e move up
bindsym $MOD+Shift+i move right

mode "resize" {
    bindsym Escape mode default

    bindsym h resize shrink width 5 px or 5 ppt
    bindsym n resize grow height 5 px or 5 ppt
    bindsym e resize shrink height 5 px or 5 ppt
    bindsym i resize grow width 5 px or 5 ppt
    bindsym h+shift resize shrink width 10 px or 10 ppt
    bindsym n+shift resize grow height 10 px or 10 ppt
    bindsym e+shift resize shrink height 10 px or 10 ppt
    bindsym i+shift resize grow width 10 px or 10 ppt
}
