### zsh
#cd /etc
#echo "#!/usr/bin/zsh" > zshrc
#echo "\n" >> zshrc
#echo "# Only execute this file once per shell." >> zshrc
#echo 'if [ -n "$__ETC_ZSHRC_SOURCED" -o -n "$NOSYSZSHRC" ]; then return; fi' >> zshrc
#echo "__ETC_ZSHRC_SOURCED=1" >> zshrc
#echo "\n" >> zshrc
#echo ". /etc/zinputrc" >> zshrc
#echo "\n" >> zshrc
#echo "# Environment variables" >> zshrc
#echo '. "/home/cvoges12/.nix-profile/etc/profile.d/hm-session-vars.sh"' >> zshrc
#echo "\n" >> zshrc
#echo "# Tell zsh how to find installed completions" >> zshrc
#echo "for profile in ${(z)NIX_PROFILES}; do" >> zshrc
#echo "    fpath+=($profile/share/zsh/site-functions $profile/share/zsh/$ZSH_VERSION/functions $profile/share/zsh/vendor-completions)" >> zshrc
#echo "done" >> zshrc
#echo "\n" >> zshrc
#echo "# history defaults" >> zshrc
#echo "SAVEHIST=1024" >> zshrc
#echo "HISTSIZE=1024" >> zshrc
#echo "HISTFILE=$HOME/.histfile\n" >> zshrc
#echo "setopt HIST_FCNTL_LOCK" >> zshrc
#echo "setopt HIST_IGNORE_DUPS" >> zshrc
#echo "setopt HIST_EXPIRE_DUPS_FIRST" >> zshrc
#echo "setopt SHARE_HISTORY" >> zshrc
#echo "setopt EXTENDED_HISTORY" >> zshrc
#echo 'mkdir -p "$(dirname "$HISTFILE")"' >> zshrc
#echo "setopt HIST_IGNORE_DUPS SHARE_HISTORY HIST_FCNTL_LOCK\n" >> zshrc
#echo "\n" >> zshrc
#echo "# Autocompletion" >> zshrc
#echo "autoload -U compinit && compinit" >> zshrc
#echo "\n" >> zshrc
#echo "# Bind gpg-agent to this TTY if gpg commands are used." >> zshrc
#echo "export GPG_TTY=$(tty)" >> zshrc
#echo "\n" >> zshrc
#echo "# Read system-wide modifications." >> zshrc
#echo "if test -f /etc/zshrc.local; then" >> zshrc
#echo "    . /etc/zshrc.local" >> zshrc
#echo "fi" >> zshrc
#echo "\n" >> zshrc
#echo "typeset -U path cdpath fpath manpath" >> zshrc
#echo "autoload -U colors && colors" >> zshrc
#echo "export PROMPT='%n@%m:%~%# '" >> zshrc
#echo "(cat .cache/wal/sequences &)" >> zshrc
#echo "\n" >> zshrc
#echo "# Aliases" >> zshrc
#echo "## Package Management" >> zshrc
#echo 'alias nadd="nix-env -i"' >> zshrc
#echo 'alias xadd="xbps-install -S"' >> zshrc
#echo 'alias aadd="apk add"' >> zshrc
#echo 'alias nrm="nix-env -e"' >> zshrc
#echo 'alias xrm="xbps-remove -R"' >> zshrc
#echo 'alias arm="apk del"' >> zshrc
#echo 'alias ndate="nix-env -u"' >> zshrc
#echo 'alias xdate="xbps-install -Su"' >> zshrc
#echo 'alias adate="apk update' >> zshrc
#echo 'alias ngrade="nix-channel --update && nix-env -iA nixpkgs.nix"' >> zshrc
#echo '# xbps is rolling release' >> zshrc
#echo 'alias agrade="apk upgrade"' >> zshrc
#echo 'alias nquery="nix-env -qa"' >> zshrc
#echo 'alias xquery="xbps-query -Rs"' >> zshrc
#echo 'alias aquery="apk search"' >> zshrc
#echo 'alias nls="nix-env -q"' >> zshrc
#echo 'alias xls="xbps-query -l"' >> zshrc
#echo 'alias als="apk info"' >> zshrc
#echo "\n" >> zshrc

### xorg
xbps-install -Sy xorg-minimal xorg-fonts xf86-input-synaptics xf86-video-intel
nix-env -i haskellPackages.xmonad kitty
