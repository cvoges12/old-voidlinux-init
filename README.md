# Install for void



### Configuration

Each .conf file is read and used to tweak different parts of the 
installation ahead of time. Except where explicitly told through 
comments, these are exactly the same as the dotfiles or .config file 
that you'd expect. Some parts might be added in later. When in doubt,
read the source code.



### For Developers

Remember to reset credentials to empty string before commiting. This includes:
- `USERN` in user.conf
- `USERPW` in user.conf
- `ROOTPW` in root.conf
