#!/bin/bash
root(){
    source root.conf
    (echo $ROOTPW && echo $ROOTPW) | passwd root
    chown root:root /
    chmod 755 /
    xbps-install -Syu
    xbps-install -Syu
    for i in $PKGS
    do
        xbps-install -Sy $i
    done
}

etc(){
    resolv(){
        chmod +r /etc/resolv.conf
        #echo "" > /etc/resolv.conf
        #for i in $(cat dns.conf)
        #do
        #    echo "nameserver ${i}" >> /etc/resolv.conf
        #done
    }

    hostname(){
        export HOST=$(cat hostname.conf)
        echo "$HOST" > /etc/hostname
        echo "127.0.0.1 $HOST" > /etc/hosts
        echo "::1 $HOST" >> /etc/hosts
    }
    
    rc(){
        rc_conf(){
            echo 'echo "### running rc.conf ###"' >> /etc/rc.conf
            echo HOSTNAME="$HOST" > /etc/rc.conf
            echo 'HARDWARECLOCK="utc"' >> /etc/rc.conf
            echo 'TIMEZONE="UTC"' >> /etc/rc.conf
            echo 'KEYMAP="us"' >> /etc/rc.conf
            echo '# console font to load. See setfont(8)' >> /etc/rc.conf
            echo '#FONT="lat9w-16"' >> /etc/rc.conf
            echo '# console map to load. See setfont(8)' >> /etc/rc.conf
            echo '#FONT_MAP=' >> /etc/rc.conf
            echo '# font unimap to load. See setfont(8)' >> /etc/rc.conf
            echo '#FONT_UNIMAP=' >> /etc/rc.conf
            echo '# kernel modules to load. Separate by spaces' >> /etc/rc.conf
            echo '#MODULES=""' >> /etc/rc.conf
        }
        
        rc_local(){
            echo 'echo "### running rc.local ###"' >> /etc/rc.local
            [ $(grep -v '#' network.conf \
                | grep -v -e "^$") == "static" ] \
                && echo 'ip link set dev eth0' >> /etc/rc.local \
                && echo 'ip addr add 192.168.1.10/24 brd + dev eth0' >> /etc/rc.local \
                && echo 'ip route add default via 192.168.1.254' >> /etc/rc.local
    
            [ $(grep -v '#' network.conf \
                | grep -v -e "^$") != "static" ] \
                && echo '#ip link set dev eth0' >> /etc/rc.local \
                && echo '#ip addr add 192.168.1.10/24 brd + dev eth0' >> /etc/rc.local \
                && echo '#ip route add default via 192.168.1.254' >> /etc/rc.local
        }
        
        rc_shutdown(){
            echo 'echo "### running rc.shutdown ###"' > /etc/rc.shutdown
            echo 'echo "...nighty night"' >> /etc/rc.shutdown
        }
        
        echo "running rc_conf" && rc_conf && echo "running rc_local" && rc_local && echo "running rc_shutdown" && rc_shutdown
    }
    
    services(){
        misc(){
            for i in alsa chronyd cronie cupsd dbus sshd uuidd
            do
                ! [ -f /etc/runit/runsvdir/current/${i} ] \
                    && ln -s /etc/sv/${i} /etc/runit/runsvdir/current
            done
        }
        
        network(){
            dhcp(){
                [ $(grep -v '#' network.conf \
                    | grep -v -e "^$") == "dhcpcd" ] \
                    && ln -s /etc/sv/dhcpcd /etc/runit/runsvdir/current
            }

            nm(){
                [ $(grep -v '#' network.conf \
                    | grep -v -e "^$") == "NetworkManager" ] \
                    && xbps-install -Sy NetworkManager \
                    && ln -s /etc/sv/NetworkManager /etc/runit/runsvdir/current
            }
            # Static is in rc_local
            dhcp || nm
        }

        agetty(){
            for i in $(seq 3 6)
            do
                touch /etc/sv/agetty-tty${i}/down
                rm /etc/runit/runsvdir/default/agetty-tty${i}
            done
        }

        misc && network && agetty
    }
    
    fstab(){
        echo "UUID=$(blkid \
            | grep "sda1" \
            | sed "s/^.*\ UUID=\"//" \
            | sed "s/\".*$//") / ext4 defaults,noatime,nodiratime,discard 0 1" >> /etc/fstab

        echo "UUID=$(blkid \
            | grep "sda2" \
            | sed "s/^.*\ UUID=\"//" \
            | sed "s/\".*$//") none swap sw 0 0" >> /etc/fstab
    }
    
    glibc(){
        ###DONT USE WITH MUSL###
        echo "en_US.UTF-8 UTF-8" >> /etc/default/libc-locales
        xbps-reconfigure -f glibc-locales
    }
    
    dracut(){
        echo '#logfile=/var/log/dracut.log' >> /etc/dracut.conf
        echo '#fileloglvl=6' >> /etc/dracut.conf
        echo -e "\n" >> /etc/dracut.conf
        echo '# Exact list of dracut modules to use.  Modules not listed here are not going' >> /etc/dracut.conf
        echo '# to be included.  If you only want to add some optional modules use' >> /etc/dracut.conf
        echo '# add_dracutmodules option instead.' >> /etc/dracut.conf
        echo '#dracutmodules+=""' >> /etc/dracut.conf
        echo -e "\n" >> /etc/dracut.conf
        echo '# dracut modules to omit' >> /etc/dracut.conf
        echo '#omit_dracutmodules+=""' >> /etc/dracut.conf
        echo -e "\n" >> /etc/dracut.conf
        echo '# dracut modules to add to the default' >> /etc/dracut.conf
        echo '#add_dracutmodules+=""' >> /etc/dracut.conf
        echo -e "\n" >> /etc/dracut.conf
        echo '# additional kernel modules to the default' >> /etc/dracut.conf
        echo '#add_drivers+=""' >> /etc/dracut.conf
        echo -e "\n" >> /etc/dracut.conf
        echo '# list of kernel filesystem modules to be included in the generic initramfs' >> /etc/dracut.conf
        echo '#filesystems+=""' >> /etc/dracut.conf
        echo -e "\n" >> /etc/dracut.conf
        echo '# build initrd only to boot current hardware' >> /etc/dracut.conf
        echo 'hostonly="yes"' >> /etc/dracut.conf
        echo -e "\n" >> /etc/dracut.conf
        echo '# install local /etc/mdadm.conf' >> /etc/dracut.conf
        echo '#mdadmconf="no"' >> /etc/dracut.conf
        echo -e "\n" >> /etc/dracut.conf
        echo '# install local /etc/lvm/lvm.conf' >> /etc/dracut.conf
        echo '#lvmconf="no"' >> /etc/dracut.conf
        echo -e "\n" >> /etc/dracut.conf
        echo '# A list of fsck tools to install. If it's not specified, module's hardcoded' >> /etc/dracut.conf
        echo '# default is used, currently: "umount mount /sbin/fsck* xfs_db xfs_check' >> /etc/dracut.conf
        echo '# xfs_repair e2fsck jfs_fsck reiserfsck btrfsck". The installation is' >> /etc/dracut.conf
        echo '# opportunistic, so non-existing tools are just ignored.' >> /etc/dracut.conf
        echo '#fscks=""' >> /etc/dracut.conf
        echo -e "\n" >> /etc/dracut.conf
        echo '# inhibit installation of any fsck tools' >> /etc/dracut.conf
        echo '#nofscks="yes"' >> /etc/dracut.conf
        echo -e "\n" >> /etc/dracut.conf
        echo '# mount / and /usr read-only by default' >> /etc/dracut.conf
        echo '#ro_mnt="no"' >> /etc/dracut.conf
        echo -e "\n" >> /etc/dracut.conf
        echo '# set the directory for temporary files' >> /etc/dracut.conf
        echo '# default: /var/tmp' >> /etc/dracut.conf
        echo 'tmpdir=/tmp' >> /etc/dracut.conf
    }
    
    resolv && hostname && rc && services && fstab
}

kernel(){
    xbps-install -Syu
    xbps-install -Sy linux
}

grub(){
    cd /boot
    xbps-install -Sy grub
    #echo 'net.ifnames=0' >> /etc/default/grub
    grub-mkconfig -o grub/grub.cfg
    grub-install /dev/sda
}

firmware(){
    cd /
    for i in $(grep -v "#" firmware.conf | grep -v -e '^$');
    do
        xbps-install -Sy $i
    done
}

x11(){
    cd /
    xbps-install -Sy xorg-minimal xorg-fonts
    drivers(){
        for i in $(grep -v "#" drivers.conf | grep -v -e '^$');
        do
            xbps-install -Sy $i
        done
    }
    drivers
}

user(){
    echo "%wheel ALL=(ALL) ALL" >> /etc/sudoers
    source user.conf
    useradd -m -s /bin/bash -U -G \
        kmem,wheel,tty,tape,floppy,disk,lp,dialout,audio,video,utmp,cdrom,optical,storage,network,kvm,input,users \
        $USERN

    (echo $USERPW && echo $USERPW) | passwd $USERN
    echo startx >> /home/$USERN/.bash_profile
    chown cvoges12 user.sh
    xbps-install -Sy curl xz    # for user_sh.nix
    #sudo -H -u cvoges12 bash -c '/user.sh'
}

reconfigure(){
    xbps-reconfigure -fa
}

root && etc && kernel && grub && firmware && x11 && user && reconfigure
