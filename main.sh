disk(){
    xbps-install -Suy
    xbps-install -Sy parted
    parted -s /dev/sda \
        mklabel msdos \
        'mkpart primary ext4 1MiB -8GiB' \
        'mkpart primary linux-swap -8GiB 100%' \
        set 1 boot on
}

swap(){
    mkswap -L SWAP /dev/sda2
    swapon /dev/sda2
}

root(){
    mkdir -p /mnt \
        && (echo "y" | mke2fs -t ext4 -L ROOT /dev/sda1) \
        && mount /dev/sda1 /mnt
}

system(){
    xbps-install -Suy
    xbps-install -Suy
    ARCH=x86_64
    LINK="https://alpha.de.repo.voidlinux.org/live/20191109/"
    TARBALL="void-x86_64-musl-ROOTFS-20191109.tar.xz"
    XBPS_ARCH=$ARCH
    xbps-install -Sy wget xz
    wget "${LINK}${TARBALL}"
    tar xvf "${TARBALL}" -C /mnt
}

mk_chroot(){
    cd /mnt
    mount -t proc /proc proc
    mount -t sysfs /sys sys
    mount -o bind /dev dev
    mount -t devpts pts dev/pts
    mv /root/* .
    cp /etc/resolv.conf ./etc/resolv.conf
    chroot . /usr/bin/bash ./chroot.sh
}

disk && swap && root && system && mk_chroot
