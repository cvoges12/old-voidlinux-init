#source user.conf
#nix(){ 
#    echo -e "\ninstalling nix\n"
#    bash <(echo $USERPW | sudo -s curl https://nixos.org/nix/install)
#    . /home/$USER/.nix-profile/etc/profile.d/nix.sh
#}

#i3(){
#    echo -e "\ninstalling i3\n"
#    nix-env -i i3-gaps
#    source kb.conf
#    [ "$KB" == "qwerty" ] && cat qwerty.conf >> i3.conf
#    [ "$KB" == "colemak" ] && cat colemak.conf >> i3.conf
#    mkdir -p /home/$USERN/.config/i3/config
#    cat i3.conf >> /home/$USERN/.config/i3/config
#    echo exec i3 >> /home/$USER/.xinit
#}

pb(){
    echo -e "\ninstalling polybar\n"
    nix-env -i polybar
    read -r -d '' BARS <<'EOP'
[bar/bottom0]
background=${colors.background}
bottom=true
fixed-center=true
font-0=Fira Mono:pixelsize=10;0
font-1=FontAwesome5Free:pixelsize=10;0
font-2=FontAwesome5Free:style=Solid:pixelsize=10;0
font-3=WenQuanYi Zen Hei:pixelsize=10;0
foreground=${colors.foreground}
height=3%
line-size=2
module-margin-left=2
module-margin-right=2
modules-center=mpd
modules-left=wired wifi
modules-right=volume light date
EOP
    echo $BARS >> polybar.conf
    
    read -r -d '' BARS <<EOP
monitor=$(xrandr | grep "\ connected" | awk '{print $1}')
EOP
    echo $BARS >> polybar.conf
    
    read -r -d '' BARS <<'EOP'
monitor-strict=true
padding=2
radius=0
width=100%

[bar/top0]
background=${colors.background}
bottom=false
fixed-center=true
font-0=Fira Mono:pixelsize=10;0
font-1=FontAwesome5Free:pixelsize=10;0
font-2=FontAwesome5Free:style=Solid:pixelsize=10;0
font-3=WenQuanYi Zen Hei:pixelsize=10;0
foreground=${colors.foreground}
height=3%
line-size=2
module-margin-left=2
module-margin-right=2
modules-left=i3
modules-right=cpu memory fs temp battery
EOP
    echo $BARS >> polybar.conf
    
    read -r -d '' BARS <<EOP
monitor=$(xrandr | grep "\ connected" | awk '{print $1}')
EOP
    echo $BARS >> polybar.conf
    
    read -r -d '' BARS <<'EOP'
monitor-strict=true
padding=2
radius=0
width=100%
EOP
    echo $BARS >> polybar.conf
}

#nix && i3 && 
polybar
